import * as React from 'react';
import {Button, View} from 'react-native';
import {createDrawerNavigator, DrawerItem} from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Home from '../Views/Home';
import Shopping from '../Views/shopping';
import Sidebar from '../Components/sidebar';
import itemScreen from '../Views/ItemDetail';
import OrderScreen from '../Views/orders';
import styles from '../Views/Home/styles';
import Login from '../Views/Login';

const Stack = createStackNavigator();

function MyStack() {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="Shopping" component={Shopping} />
      <Stack.Screen name="itemScreen" component={itemScreen} />
      <Stack.Screen name="orderScreen" component={OrderScreen} />
    </Stack.Navigator>
  );
}

const Drawer = createDrawerNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Drawer.Navigator 
        drawerContent={props => <Sidebar {...props} />}
        initialRouteName="Login"
        screenOptions={{headerShown: false}}>
        <Drawer.Screen name="Home" component={Home} />
        <Drawer.Screen name="ShoppingScreen" component={MyStack} />
        <Drawer.Screen name="Login" component={Login} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}
