import SETTINGS from '../Resources/API';

const Ecommerces = {
  async getAllProducts() {
    return fetch(`${SETTINGS.api.dev}products`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },
  async getcategories() {
    return fetch(`${SETTINGS.api.dev}products/categories`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },
};

export default Ecommerces;
