import React, {useEffect, useState, useRef} from 'react';
import {View, Image, ScrollView, FlatList, Animated} from 'react-native';
import styles from './styles';
import {Card, Text} from 'react-native-elements';
import {NavigationStackScreenProps} from 'react-navigation-stack';
import Header from '../../Components/Header';
import * as Animatable from 'react-native-animatable';
import {withEcommerceContext} from '../../Context/whitEcommerce';
import Loader from '../../Components/Loader';
import Carousel from '../../Components/Carousel/Carousel';
import CategoriesCarousel from '../../Components/Carousel/Categories';
import ItemDetail from '../ItemDetail';

interface Props extends NavigationStackScreenProps {
  context: any;
}
const Home = (props: Props) => {
  const provider = props.context;
  const state = provider.state;
  const navigation = props.navigation;
  const [isModalVisible, setisModalVisible] = useState(false);
  useEffect(() => {
    provider.getAllProductsFunc();
    provider.getCategories();
  }, []);

  useEffect(() => {}, [state]);

  return (
    <View style={{flex: 1, backgroundColor: '#f1f1f1'}}>
      <Header title="BetterWare" navigation={navigation} />
      <Loader loading={state.loading} />
      <Text
        style={{
          padding: 10,
          textAlign: 'left',
          fontSize: 20,
          fontWeight: 'bold',
        }}>
        Categories
      </Text>
      <ScrollView horizontal style={{maxHeight: 70}}>
        {state.categories.map(i => (
          <CategoriesCarousel key={i} item={i} />
        ))}
      </ScrollView>
      <Text
        style={{
          padding: 10,
          textAlign: 'left',
          fontSize: 20,
          fontWeight: 'bold',
        }}>
        Best sellers
      </Text>
      <View style={styles.container}>
        <FlatList
          data={state.products}
          keyExtractor={item => item.id}
          renderItem={({item, index}) => (
            <Carousel
              key={index}
              products={item}
              {...props}
              navigation={navigation}
            />
          )}
          horizontal={false}
          numColumns={2}
        />
      </View>
    </View>
  );
};

export default withEcommerceContext(Home);
