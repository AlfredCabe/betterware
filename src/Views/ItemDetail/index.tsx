import React, {useEffect, useState, useRef} from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableHighlight,
  StyleSheet,
  Image,
} from 'react-native';
import Modal from 'react-native-modal';
import Header from '../../Components/Header';
import Feather from 'react-native-vector-icons/Feather';
import {Button} from 'react-native-elements';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ModalComponent from '../../Components/Modals/BuyModal';

const itemDetail = props => {
  const {navigation, route} = props;
  const [product, setProduct] = useState({} as any);
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    setProduct(route.params.products);
  }, [route]);

  const addToBag = async () => {
    let cart = JSON.parse(await AsyncStorage.getItem('cart'));
    let newCart = [];
    if (cart != null) {
      let array = [...cart];
      if (cart.length > 0) {
        const exist = cart.filter(i => i.id == product.id);
        if (exist.length == 0) {
          array.push(product);
          AsyncStorage.setItem('cart', JSON.stringify(array));
        }
      } else {
        newCart.push(product);
        AsyncStorage.setItem('cart', JSON.stringify(newCart));
      }
    } else {
      newCart.push(product);
      AsyncStorage.setItem('cart', JSON.stringify(newCart));
    }
    navigation.navigate('ShoppingScreen', {
      screen: 'Shopping',
    })
  };
  return (
    <View style={{backgroundColor: '#FFF', flex: 1}}>
      <Header hasBack title="Product Detail" navigation={navigation} detail />
      <View style={{width: '100%', height: 400, backgroundColor: '#FFF'}}>
        <Image
          source={{uri: product.image}}
          style={{width: '100%', height: '100%'}}
          resizeMode="contain"
        />
      </View>
      <View
        style={{
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
          padding: 20,
          backgroundColor: '#f1f1f1',
          flex: 1,
        }}>
        <View
          style={{
            borderBottomColor: '#777',
            borderBottomWidth: 1,
            marginBottom: 40,
          }}>
          <Text style={{fontSize: 15, fontWeight: '500', paddingBottom: 10}}>
            {product.title}
          </Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: 15,
                color: '#777',
                fontWeight: 'bold',
                paddingBottom: 10,
              }}>
              $ {product.price}
            </Text>
          </View>
        </View>
        <View style={styles.fixToText}>
          <Button
            title="ADD TO BAG"
            type="outline"
            containerStyle={{width: '50%'}}
            titleStyle={{color: '#777'}}
            buttonStyle={{height: 55, backgroundColor: 'black', marginRight: 5}}
            onPress={addToBag}
          />
          <Button
            title="BUY"
            type="outline"
            containerStyle={{width: '50%'}}
            titleStyle={{color: '#777'}}
            buttonStyle={{height: 55, borderColor: 'black', marginRight: 5}}
            onPress={() => setVisible(!visible)}
          />
        </View>
        <ModalComponent visible={visible} setVisible={setVisible} navigation={navigation} />
      </View>
    </View>
  );
};

export default itemDetail;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
