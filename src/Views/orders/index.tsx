import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  TouchableHighlight,
  Image,
  StyleSheet,
  ScrollView,
} from 'react-native';
import Header from '../../Components/Header';
import {withEcommerceContext} from '../../Context/whitEcommerce';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {Button} from 'react-native-elements';

const OrderScreen = props => {
  const provider = props.context;
  const state = provider.state;
  const navigation = props.navigation;
  const [isModalVisible, setisModalVisible] = useState(false);

  useEffect(() => {}, []);

  return (
    <View>
      <Header hasBack title="Orders" navigation={navigation} detail />
      <ScrollView >
        <View style={styles.rowFront}>
          <View
            style={{
              backgroundColor: '#FFF',
              flexDirection: 'row',
            }}>
            <Image
              source={require('../../Assets/dress.jpeg')}
              style={{width: 150, height: 150}}
              resizeMode={'contain'}
            />
            <View style={{width: '50%', padding: 20, flexDirection: 'column'}}>
              <Text>Medium pink lady dress</Text>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'flex-end',
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <Text style={{}}>$ 120</Text>
                </View>
              </View>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text style={{paddingLeft: 30}}>Total: 129.00</Text>
            <Button
              title={'REORDER'}
              titleStyle={{fontWeight: 'bold'}}
              buttonStyle={{
                height: 55,
                borderRadius: 10,
                backgroundColor: '#000',
              }}></Button>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  card: {
    flex: 1,
    borderRadius: 4,
    borderWidth: 2,
    borderColor: '#E8E8E8',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  text: {
    textAlign: 'center',
    fontSize: 50,
    backgroundColor: 'transparent',
  },
  rowFront: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    height: hp(30),
    margin: 10,
    borderRadius: 10,
    padding: 10,
    width: '95%',
    flexDirection: 'column',
  },
  rowBack: {
    alignItems: 'flex-end',
    justifyContent: 'center',
    backgroundColor: '#f1f1f1',
    flex: 1,
    paddingRight: 15,
  },
});

export default withEcommerceContext(OrderScreen);
