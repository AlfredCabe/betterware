import React from 'react';
import {Text, View, ImageBackground} from 'react-native';
import { Input, Center, NativeBaseProvider,  } from "native-base"
import {Button} from 'react-native-elements';
const Login = (props) => {
  const {navigation} = props
  return (
    <ImageBackground source={require('../../Assets/background.jpeg')} style={{flex: 1, justifyContent: 'center', alignItems:'center'}}>
      <Text style={{fontSize: 50, fontWeight: 'bold', marginBottom: 30, letterSpacing: 20}}>Login</Text>
      <Input
        w="90%"
        mx={3}
        placeholder="User"
        placeholderTextColor={'#000000'}
        fontSize={20}
        opacity={.7}
        backgroundColor="#FFF"
        borderRadius={10}
        marginBottom={10}
        _dark={{
          placeholderTextColor: 'blueGray.900',
        }}      
      />
      <Input
        w="90%"
        marginBottom={10}
        mx={3}
        borderRadius={10}
        opacity={.7}
        placeholder="Password"
        backgroundColor="#FFF"
        placeholderTextColor={'#000000'}
        fontSize={20}
      />
           <Button
            title="Log In"
            type="outline"
            containerStyle={{width: '50%'}}
            titleStyle={{color: '#FFF'}}
            buttonStyle={{height: 55, backgroundColor: 'black', marginRight: 5, borderRadius: 50}}
            onPress={() => navigation.navigate('Home')}
          />
    </ImageBackground>
  );
};

export default Login;
