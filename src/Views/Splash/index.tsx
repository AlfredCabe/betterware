import React from 'react';
import {
  Text,
  View,
  Image
} from 'react-native';

const Splash = () => {

  return (
      <View style={{flex: 1, alignContent: 'center', alignItems: 'center'}}>
          <Image source={require('../../Assets/splash.png')}/>
      </View>
  );
};


export default Splash;
