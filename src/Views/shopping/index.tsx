import React, {useEffect, useState, useRef} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableHighlight,
  ScrollView,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import Header from '../../Components/Header';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Button} from 'react-native-elements';
import Feather from 'react-native-vector-icons/Feather';
import {SwipeListView} from 'react-native-swipe-list-view';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import ModalComponent from '../../Components/Modals/BuyModal';

const Shopping = props => {
  const navigation = props.navigation;
  const [cartList, setCartList] = useState([]);
  const [visible, setVisible] = useState(false);
  async function fetchMyAPI() {
    let cart = JSON.parse(await AsyncStorage.getItem('cart'));
    setCartList(cart);
    console.log(cart);
    
  }
  const handleDelete = async data => {
    const {item, index} = data;
    let cart = JSON.parse(await AsyncStorage.getItem('cart'));
    cart = cart.filter(function (i) {
      return i.id !== item.id;
    });
    AsyncStorage.setItem('cart', JSON.stringify(cart));
    fetchMyAPI();
  };
  useEffect(() => {
    fetchMyAPI();
    console.log('cartList', cartList);
  }, []);
  return (
    <View style={{flex: 1}}>
      <Header title="BetterWare" navigation={navigation} />
      {/* <ScrollView contentContainerStyle={{flex: 1, paddingBottom: '25%'}}> */}
      <SwipeListView
        style={{marginBottom: hp(10)}}
        data={cartList}
        renderItem={(data, rowMap) => (
          <TouchableHighlight style={styles.rowFront}>
            <View
              style={{
                backgroundColor: '#FFF',
                flexDirection: 'row',
              }}>
              <Image
                source={{uri: data.item.image}}
                style={{width: 150, height: 150}}
                resizeMode={'contain'}
              />
              <View
                style={{width: '50%', padding: 20, flexDirection: 'column'}}>
                <Text>{data.item.title}</Text>
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'flex-end',
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <Text style={{}}>$ {data.item.price}</Text>
                    <Text>Rate: {data.item.rating.rate}</Text>
                  </View>
                </View>
              </View>
            </View>
          </TouchableHighlight>
        )}
        renderHiddenItem={(data, rowMap) => (
          <TouchableHighlight
            style={{flex: 1}}
            onPress={() => handleDelete(data)}>
            <View style={styles.rowBack}>
              <Feather name="trash-2" size={40} color="red" />
            </View>
          </TouchableHighlight>
        )}
        leftOpenValue={0}
        rightOpenValue={-125}
      />
      {/* </ScrollView> */}
      <View style={{position: 'absolute', bottom: 40, left: 20, right: 20}}>
        <Button
          title={'BUY NOW'}
          buttonStyle={{
            height: 55,
            borderRadius: 50,
            backgroundColor: '#000',
          }}
          onPress={() => setVisible(!visible)}></Button>
      </View>
      <ModalComponent
        visible={visible}
        setVisible={setVisible}
        navigation={navigation}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  card: {
    flex: 1,
    borderRadius: 4,
    borderWidth: 2,
    borderColor: '#E8E8E8',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  text: {
    textAlign: 'center',
    fontSize: 50,
    backgroundColor: 'transparent',
  },
  rowFront: {
    alignItems: 'center',
    backgroundColor: '#fff',
    justifyContent: 'center',
    height: hp(30),
    margin: 10,
    borderRadius: 10,
    padding: 10,
    width: '95%',
  },
  rowBack: {
    alignItems: 'flex-end',
    justifyContent: 'center',
    backgroundColor: '#f1f1f1',
    flex: 1,
    paddingRight: 15,
  },
});

export default Shopping;
