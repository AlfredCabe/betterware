import {StyleSheet, Dimensions} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const {width: screenWidth} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  item: {
    width: screenWidth - 10,
    height: screenWidth - 10,
    padding: 20,
    backgroundColor: '#FFF',
    borderRadius: 20,
  },
  imageContainer: {
    flex: 1,
    marginBottom: Platform.select({ios: 0, android: 1}), // Prevent a random Android rendering issue
    backgroundColor: 'transparent',
    borderRadius: 20,
  },
  image: {
    ...StyleSheet.absoluteFillObject,
    resizeMode: 'contain',
  },
  title: {
    fontSize: 30,
    alignSelf: 'center',
    marginTop: hp(5),
    fontWeight: 'bold',
    color: '#FFF'
  },
});

export default styles;

