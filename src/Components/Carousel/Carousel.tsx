import React, {useRef, useState, useEffect} from 'react';
import Carousel, {ParallaxImage} from 'react-native-snap-carousel';
import * as Animatable from 'react-native-animatable';
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Image,
  Animated,
  FlatList,
} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import {Capitalize} from '../../Resources/Functions';
let { width, height } = Dimensions.get("window");
let SQUARE_DIMENSIONS = 30;
let SPRING_CONFIG = { tension: 2, friction: 3 }; //Soft spring

const MyCarousel = ({products, navigation}) => {
  const [entries, setEntries] = useState([]);
  const AnimationRef = useRef(null);
  const [animation] = useState(new Animated.Value(1));
  const _handleSend = item => {

    if (AnimationRef) {
      AnimationRef.current?.fadeOutUpBig();
    }
  };

  const startAnimation = (products) => {
    const animations = [
      Animated.timing(animation, {
        useNativeDriver: false,
        ...SPRING_CONFIG,
        toValue: 1.5,
        duration: 500,
      }),
      Animated.timing(animation, {
        useNativeDriver: false,
        ...SPRING_CONFIG,
        toValue: 1,
        duration: 500,
      }),
    ];
    
    Animated.sequence(animations).start();
    setTimeout(() => {
      navigation.navigate("ShoppingScreen", {
        screen: "itemScreen",
        params: { products: products},
      })
    }, 200);
  };

  useEffect(() => {
    setEntries(products);
  }, [products, entries]);

  return (
    <Animated.View
      key={products.id}
      ref={AnimationRef}
      style={{
        backgroundColor: '#FFF',
        width: 170,
        margin: 10,
        padding: 5,
        borderRadius: 20,
        transform: [{scale: animation}],
      }}>
      <TouchableOpacity onPress={() => startAnimation(products)}>
        <Image
          source={{uri: products.image}}
          resizeMode="contain"
          style={{width: 150, height: 200}}
        />
      </TouchableOpacity>
      <Text style={{fontWeight: 'bold', paddingTop: 5}}>${products.price}</Text>
      <Text style={{fontSize: 15, color: '#cbcbcb'}}>
        {products.title.substring(0, 25)}...
      </Text>
    </Animated.View>
  );
};
MyCarousel.propTypes = {
  products: PropTypes.object,
  handleSend: PropTypes.func,
};

MyCarousel.defaultProps = {
  products: undefined,
  handleSend: undefined,
};

export default MyCarousel;
