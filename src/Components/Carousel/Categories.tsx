import React, {useRef, useState, useEffect} from 'react';
import Carousel, {ParallaxImage} from 'react-native-snap-carousel';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Platform,
} from 'react-native';
import {Card} from 'react-native-elements'
import { heightPercentageToDP, widthPercentageToDP  } from 'react-native-responsive-screen';
import { Capitalize } from '../../Resources/Functions';
const {width: screenWidth} = Dimensions.get('window');

const CategoriesCar = props => {
  const {item} = props;
  const [entries, setEntries] = useState([]);
  const carouselRef = useRef(null);

  const goForward = () => {
    carouselRef.current.snapToNext();
  };

  useEffect(() => {
    setEntries(item);
  }, []);


  return (
    <Card containerStyle={styles.container}>
      <TouchableOpacity>
        <Text style={{textAlign:'center'}}>{Capitalize(entries)}</Text>
      </TouchableOpacity>
    </Card>
  );
};

export default CategoriesCar;

const styles = StyleSheet.create({
  container: {
    width: widthPercentageToDP('90%'),
    height: 50,
    borderRadius: 10
  },
});
