import React from 'react';
import {Header} from 'react-native-elements';
import {TouchableWithoutFeedback, View} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import PropTypes from 'prop-types';
import {Badge} from 'native-base';

const HeaderComponenet = props => {
  const {hasBack, title, navigation, detail} = props;
  const BackScreen = () => {
    navigation.navigate('Home');
  };
  return (
    <Header
      backgroundColor={detail ? '#FFF' : '#f1f1f1'}
      containerStyle={{
        zIndex: 1,
        borderBottomColor: detail ? 'transparent' : '#fff',
      }}
      centerComponent={{
        text: title,
        style: {
          color: '#323232',
          fontWeight: 'bold',
          fontSize: 20,
          marginBottom: 5,
        },
      }}
      leftComponent={
        hasBack ? (
          <TouchableWithoutFeedback onPress={() => BackScreen()}>
            <Feather color="#323232" name="arrow-left" size={25} />
          </TouchableWithoutFeedback>
        ) : (
          <TouchableWithoutFeedback onPress={() => navigation.openDrawer()}>
            <Feather color="#323232" name="menu" size={25} />
          </TouchableWithoutFeedback>
        )
      }
      rightComponent={
        <TouchableWithoutFeedback
          onPress={() =>
            navigation.navigate('ShoppingScreen', {
              screen: 'Shopping',
            })
          }>
          <View>
            <Badge
              style={{
                width: 2,
                height: 10,
                position: 'absolute',
                top: -2,
                right: 1,
                zIndex: 1,
              }}
              colorScheme="primary"
              ml={1}
              rounded="md"
            />
            <Feather
              style={{zIndex: 0}}
              color="#323232"
              name="shopping-cart"
              size={25}
            />
          </View>
        </TouchableWithoutFeedback>
      }
      statusBarProps={{
        backgroundColor: '#323232',
        barStyle: 'dark-content',
        translucent: false,
      }}
      barStyle="dark-content"
    />
  );
};
HeaderComponenet.propTypes = {
  hasBack: PropTypes.bool,
  title: PropTypes.string,
  navigation: PropTypes.object,
  detail: PropTypes.bool,
};

HeaderComponenet.defaultProps = {
  hasBack: undefined,
  title: undefined,
  navigation: undefined,
  detail: undefined,
};
export default HeaderComponenet;
