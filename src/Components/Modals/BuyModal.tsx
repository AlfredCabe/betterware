import React, {useRef, useState, useEffect} from 'react';
import Carousel, {ParallaxImage} from 'react-native-snap-carousel';
import {View, Text, Dimensions, StyleSheet, Image} from 'react-native';
import {Card} from 'react-native-elements';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {Capitalize} from '../../Resources/Functions';
import Modal from 'react-native-modal';
import Feather from 'react-native-vector-icons/Feather';
const {width: screenWidth} = Dimensions.get('window');

const BuyModal = props => {
  const {visible, setVisible, navigation} = props;
  const [entries, setEntries] = useState([]);
  const carouselRef = useRef(null);

  const sendToHome = () => {
    setVisible(!visible)
    navigation.navigate("Home")
  };


  useEffect(() => {
    setEntries(visible);
  }, []);

  return (
    <View>
      <Modal isVisible={visible} onBackButtonPress={() => setVisible(!visible)}>
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <View
            style={{
              width: '100%',
              backgroundColor: '#FFF',
              borderRadius: 20,
              alignItems: 'center',
              justifyContent: 'center',
              padding: 30,
            }}>
            <View style={{position:'absolute', right: 10, top: 10}}>
              <Feather name="x-circle" size={30} color={'red'} onPress={() => sendToHome()} />
            </View>
            <Image
              source={require('../../Assets/check.png')}
              style={{width: 70, height: 70, marginBottom: 20}}
            />
            <Text
              style={{fontWeight: 'bold', fontSize: 20}}>
              Purchase Completed
            </Text>
            <Text style={{textAlign: 'center'}}>
              Thank you for creating your purchase, an email will arrive for the
              validation of your data
            </Text>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default BuyModal;

const styles = StyleSheet.create({
  container: {
    width: widthPercentageToDP('90%'),
    height: 50,
    borderRadius: 10,
  },
});
