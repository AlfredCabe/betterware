import React, {useRef, useState, useEffect} from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Platform,
} from 'react-native';
import {Avatar, Drawer} from 'native-base';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {Capitalize} from '../../Resources/Functions';
import {
  DrawerContentScrollView,
  DrawerItem,
} from '@react-navigation/drawer';
import Feather from 'react-native-vector-icons/Feather';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
const {width: screenWidth} = Dimensions.get('window');

const Sidebar = props => {
    const {navigation} = props
    
  useEffect(() => {}, []);
  return (
    <View style={styles.container}>
      <DrawerContentScrollView {...props}>
        <View style={styles.drawerContent}>
          <View style={styles.userInfoSectino}>
            <View style={{flexDirection: 'row', marginTop: 15}}>
              <Avatar
                source={{
                  uri: 'https://pbs.twimg.com/profile_images/1177303899243343872/B0sUJIH0_400x400.jpg',
                }}
              />
              <View style={{marginLeft: 15, flexDirection: 'column'}}>
                <Text style={styles.title}> Alfredo Lozoya</Text>
                <Text style={styles.subtitle}> @alfredolozoyaorozco</Text>
              </View>
            </View>
          </View>
          <View style={{marginTop: 15}}>
              <DrawerItem
                icon={({color, size}) => (
                  <Feather name="home" color={color} size={size} />
                )}
                label="Home"
                onPress={() => navigation.navigate('Home')}
              />
              <DrawerItem
                icon={({color, size}) => (
                  <Feather name="shopping-bag" color={color} size={size} />
                )}
                label="Bag"
                onPress={() => navigation.navigate('ShoppingScreen', {
                  screen: 'Shopping',
                })}
              />
              <DrawerItem
                icon={({color, size}) => (
                  <MaterialCommunityIcons name="truck-fast-outline" color={color} size={size} />
                )}
                label="Orders"
                onPress={() =>  navigation.navigate('ShoppingScreen', {
                  screen: 'orderScreen',
                })}
              />
            </View>
          
        </View>
      </DrawerContentScrollView>
      <View style={{marginBottom: 30}}>
        <DrawerItem
          icon={({color, size}) => (
            <Feather name="log-out" color={color} size={size} />
          )}
          label="Sign out"
          onPress={() => navigation.navigate('Login')}
        />
      </View>
    </View>
  );
};

export default Sidebar;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  drawerContent: {
    flex: 1,
  },
  userInfoSectino: {
    paddingLeft: 20,
  },
  title: {
    marginTop: 5,
    fontWeight: 'bold',
    fontSize: 16,
  },
  subtitle: {
    fontSize: 14,
    lineHeight: 14,
  },
});
