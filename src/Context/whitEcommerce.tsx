import React from 'react';
import {BringtheEcommerce} from './Providers/EcommerceProviders';
export function withEcommerceContext(Component: any) {
  return function WrapperComponent(props: any) {
    return (
      <BringtheEcommerce.Consumer>
        {state => <Component {...props} context={state} />}
      </BringtheEcommerce.Consumer>
    );
  };
}