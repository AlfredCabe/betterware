import React, {Component, useContext} from 'react';
import EcommerceService from '../../Services/ecommerce.service';
export const BringtheEcommerce = React.createContext({});

class EcommerceProvider extends Component {
  constructor(props: any) {
    super(props);
    this.state = {
      products: [],
      categories: [],
      loading: true
    };
  }
  getAllProductsFunc = () => {
    EcommerceService.getAllProducts()
      .then(response2 => response2.json())
      .then(response2 => {
        this.setState({products: response2, loading: false})
        
      })
      .catch(error => {
        console.error('el error de api', error);
      });
  };

  getCategories = () => {
    EcommerceService.getcategories()
    .then(response2 => response2.json())
      .then(response2 => {
        this.setState({categories: response2, loading: false})
        
      })
      .catch(error => {
        console.error('el error de api', error);
      });
  }
  

  render() {
    return (
      <BringtheEcommerce.Provider
        value={{
          state: this.state,
          getAllProductsFunc: this.getAllProductsFunc,
          getCategories: this.getCategories,
        }}>
        {this.props.children}
      </BringtheEcommerce.Provider>
    );
  }
}

export default EcommerceProvider;
